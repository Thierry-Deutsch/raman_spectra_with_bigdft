# Convergence studies

The aim of this part of the project is to define the BigDFT input parameters
giving converged results for all the molecules in our test set. The input
parameters considered here are related to the wavelet grid in real space,
namely its extension, given by rmult, and its grid step, given by hgrids.

By starting from a reference value for each of these parameters, the quality of
the job is decreased by a small amount until the total energy is not within a
given window, defined by a precision per atom (here, 1 meV per atom). Two
workflow classes of the MyBigDFT package can be used to perform that task in a
minimal amount of script code.

The relevant files here are:
- the `prepare_notebooks.ipynb` notebook. It writes a notebook for each
molecule of the test set. Running them allows to get the minimal BigDFT input
parameters (in terms of computational time) so that the total energy is below a
given precision per atom.
- the `run_notebooks.sh` script, which is used to run all the notebooks.
- the `post_processing.ipynb` notebook. It reads the desired data from the
notebooks that ran thanks to the above-mentioned script. These values are then
added to the main database of results of the project.

You should run them all in that specific order. The first two files can be
skipped, because the notebooks that ran successfully (*i.e.*, the ones with
`nbconvert` in their name) are also part of the repository. This prevents you
from having to run all the BigDFT jobs, while still having all the relevant
data at hand.
