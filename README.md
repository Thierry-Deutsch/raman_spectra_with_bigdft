# Raman spectra with BigDFT

The goal of this repository is to contain all the necessary data to reproduce
the work I did to compute the convergence of the Raman spectrum using BigDFT,
and also to compare my results to those of the literature.


## What do we have here?

The repo is mainly a collection of python notebooks, which are used to perform
every task. They are supposed to be run in that order:
- `experimental_and_theoretical_csv_files.ipynb` creates and documents all the
relevant csv files containing the experimental and theoretical data found in
the literature.
- `prepare_notebooks.ipynb` prepares all the notebooks used to perform the
study.
    * Each notebook is created from an xyz file and the value of the BigDFT
      parameters `rmult` and `hgrids` (defining the real-space grids used to
      represent the system's wavefunctions).
    * These notebooks can then be run to compute all the relevant quantities
      studied here, especially the Raman spectrum (which is the most
      time-consuming part), but also the infrared spectrum and the mean
      electronic and vibrational polarizabilities.
    * All these notebooks heavily depend on the MyBigDFT library. The latter
      provides a (hopefully) clear, concise and meaningful API to manipulate
      the BigDFT code with Python.
- OPTIONAL: `run_all_notebooks.ipynb` runs all the notebooks created by the
`prepare_notebooks.ipynb` notebook. An equivalent is the `run_notebooks.sh`
file. You may not have to run this notebook or this script if you have access
to the output notebooks, hence the OPTIONAL mention above. These files might be
included in the project in the future (in an archive); for the moment, they can
be provided upon request.
- `database_creation.ipynb` is currently used to create, document, fill
and use the database of BigDFT results as well as experimental and theoretical
results to be used as comparison.
    * sqlite3 is used to manage the database.
    * The BigDFT results are read from the output of the notebooks created by
      the `prepare_notebooks.ipynb` notebook.
    * These results are added to the database.
    * The tables are also written in csv files.
    * Experimental and theoretical data (stored in csv files) are also added to
      the database.

You must also take a look at the `convergence_studies` folder. It contains all
the desired output notebooks, so that you only have to run the
`post_processing.ipynb` notebook to make sure that the database contains the
data regarding the converged parameters for each system.
You may also want to look at the `README.md` file in that directory to have a
more detailed view of what it contains; for the moment, you may only have to
know that this directory contains all the relevant notebooks to perform the
convergence studies for all the molecules in the test set with BigDFT. The
main objective of these studies is to define the BigDFT input parameters
(namely the grid step and the grid extension) that minimize the computational
time while preserving an accuracy within 1 meV per atom with respect to a
reference calculation. These input parameters can then be used to discriminate
between "converged" and "non-converged" BigDFT calculations.

The data visualization of all the BigDFT results is presented in the
`data_visualization` folder. One notebook per quantity of interest is present
there. Their main objective is to evaluate the expected precision on the
quantity when the BigDFT input parameters total energy is precise up to at most
1 meV per atom. Many plots and stats are produced so as to help defining and
visualizing that precision.

Some initial data files are also to be found:
- the initial positions of the studied molecules are found in the
`initial_positions` directory in the form of BigDFT-compliant xyz files,
- the reference data (both experimental and theoretical) are found in the
`data_from_literature` directory in the form of csv files. These files actually
are redundant with the `experimental_and_theoretical_csv_files.ipynb` notebook,
which was used to create them.
- the `data_from_BigDFT` directory, which contains csv files that gather all
the relevant data found as output of the BigDFT calculations defined in the
`prepare_notebooks.ipynb` notebook.


## Getting started

To actually run all the BigDFT calculations, you must install BigDFT beforehand.
There are two options:
- if you have access all the BigDFT output files I created during my study,
install 1.8.3 version. Some of these output files were obtained with the
BigDFT 1.8.3 version (even though most were obtained with the 1.8.2 version).
Given that some input variables changed between both versions, an error is
raised when the BigDFT results are stored in the database in the
"" notebook.
- if you actually want to run all the calculations by yourself, you may use any
other BigDFT version, but it is recommended to use BigDFT 1.8.2 and higher, as
the MyBigDFT library was developped with BigDFT 1.8.2 and used successfully with
the 1.8.3 version as well.

Install jupyter to run all the notebooks. They might use features only present
in python 3.6+ (mainly the so-called f-strings), so make sure you have
the correct python version installed as well (and that the correct python
kernel is used by jupyter as well).


## Notes to myself and todos:

- Use the database in order to "DRY" the notebooks, expecially when it comes to
define the values of the input parameters.
- It might be nice to use repo2docker to create a Docker image I can then share.
- Add the geometry optimized files for each notebook, so as to gain some
computational time (this means that parts of the notebook preparing all the
notebooks can be commented out; I should say that at the appropriate places).
